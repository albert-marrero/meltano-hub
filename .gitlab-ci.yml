variables:
  JEKYLL_ENV: production
  LC_ALL: C.UTF-8

stages:
  - validate
  - discovery
  - test
  - deploy

##########################
# Jekyll Build
##########################

.jekyll_template: &jekyll_template
  image: ruby:2.7
  before_script:
    - bundle install

jekyll_test:
  <<: *jekyll_template
  stage: test
  needs:
    - job: discovery_validation
      artifacts: true
  script:
    - bundle exec jekyll build -d test
  artifacts:
    paths:
    - test
  only: 
    refs:
      - merge_requests

pages:
  <<: *jekyll_template
  stage: deploy
  script:
    - bundle exec jekyll build -d public
  artifacts:
    paths:
    - public
  only:
    - main
  except:
    - schedule

##########################
# Daily data pipeline
##########################

.meltano-extracts-prod:
  image: python:3.8
  variables:
    STAGE: prod
    JOB_ID: ATHENA-PROD
    DBT_TARGET: athena-prod
    TAP_REF: tap-github
    TARGET_REF: target-athena
    ATHENA_CATALOG: awsdatacatalog
    RAW_SCHEMA_NAME: prod01_raw
    DBT_OUTPUT_SCHEMA_NAME: dbt_prod
  before_script:
  - apt-get update && apt-get install -y jq
  - cd meltano/
  - pip3 install meltano
  - meltano install
  - echo "Installing creds file from CI..." && cp $MELTANO_ENV_FILE .env
  after_script:
  - |
    cd meltano
    echo "Logging current state...\n" && \
    meltano elt $TAP_REF $TARGET_REF --job_id=$JOB_ID --dump=state |  jq '.'     

.meltano-extracts-test:
  extends:
  - .meltano-extracts-prod
  variables:
    STAGE: test
    JOB_ID: ATHENA-CI-TEST
    DBT_TARGET: athena-test
    TAP_REF: tap-github-test
    TARGET_REF: target-athena-test
    ATHENA_CATALOG: awsdatacatalog
    RAW_SCHEMA_NAME: test01_raw
    DBT_OUTPUT_SCHEMA_NAME: dbt_dev

data-pipeline-test:
  extends:
  - .meltano-extracts-test
  stage: test
  only:
    changes:
      - "meltano/*"
  except:
  - schedule
  needs: []
  script:
  - meltano elt $TAP_REF $TARGET_REF --select repositories --job_id=$JOB_ID
  - meltano elt $TAP_REF $TARGET_REF --select issues --job_id=$JOB_ID
  # - meltano elt $TAP_REF $TARGET_REF --select issue_comments --job_id=$JOB_ID
  - meltano invoke dbt:run

repos-daily:
  extends:
  - .meltano-extracts-prod
  stage: deploy
  only:
  - schedule
  needs: []
  variables:
    JOB_ID: GITHUB-REPOS-PROD
    TAP_REF: tap-github
  script:
  - meltano elt $TAP_REF $TARGET_REF --select repositories --job_id=$JOB_ID

issues-daily:
  extends:
  - .meltano-extracts-prod
  stage: deploy
  only:
  - schedule
  needs: [repos-daily]  # Sequenced to avoid rate limit
  variables:
    JOB_ID: GITHUB-ISSUES-PROD
    TAP_REF: tap-github
  script:
  - meltano elt $TAP_REF $TARGET_REF --select issues --job_id=$JOB_ID

# Hourly GitHub API rate limits appear to be causing failure
# issue-comments-daily:
#   extends:
#   - .meltano-extracts-prod
#   stage: deploy
#   only:
#   - schedule
#   needs: [issues-daily]  # Sequenced to avoid rate limit
#   variables:
#     JOB_ID: GITHUB-ISSUE-COMMENTS-PROD
#     TAP_REF: tap-github
#   script:
#   - meltano elt $TAP_REF $TARGET_REF --select issue_comments --job_id=$JOB_ID

transforms-daily:
  extends:
  - .meltano-extracts-prod
  stage: deploy
  only:
  - schedule
  needs: [repos-daily, issues-daily]
  script:
  - meltano invoke dbt:run
  after_script: []

##########################
# discovery.yml Validation
##########################

discovery_validation:
  image: python:3
  before_script:
    - pip install ruamel.yaml
    - pip install deepdiff
  stage: discovery
  script:
    - curl https://gitlab.com/meltano/meltano/-/raw/master/src/meltano/core/bundle/discovery.yml --output meltano.yml
    - python discovery_yaml_generator.py
    - python yaml_diff.py
  artifacts:
    paths:
      - discovery.yml
  only: 
    refs:
      - merge_requests

##########################
# Validate taps and targets
##########################

jsonschema_validation:
  image: python:3
  before_script:
    - pip install ruamel.yaml
    - pip install jsonschema
  stage: validate
  script:
    - python yaml_validate.py
  only: 
    refs:
      - merge_requests
  except:
    - schedule
